package bcas.ap.dp.fact;
import java.util.Scanner;
public class ShapeFactoryDemo {
	
	static Scanner sc = new Scanner(System.in);
	public static void main(String[] args) {
		
		//CIRCLE
	ShapeFactory shapeFactort = new ShapeFactory();
	
	
	Shape circle= shapeFactort.callShape(ShapeType.CIRCLE);
	circle.draw();
	System.out.print("Enter the radius: ");
	
	double radius = sc.nextDouble();
	
	
	double area = Math.PI * (radius * radius);
    System.out.println("The area of circle is: " + area);
    
    double circumference= Math.PI * 2*radius;
    System.out.println( "The circumference of the circle is:"+circumference+"\n"+"\n") ;
    
    //square
	
    Shape square= shapeFactort.callShape(ShapeType.SQUARE);
	square.draw();
	System.out.println("Enter the side of the square(Hight/Width):");
	
	int len = sc.nextInt(),  bre = sc.nextInt();
    int area1 = len*bre, peri = (2*len) + (2*bre);
    	
    System.out.print("The area of square is: = " +area1);
    System.out.print("\nPerimeter = " +peri+"\n"+"\n");
	 
	 //Rectangle
	Shape reactangle= shapeFactort.callShape(ShapeType.REACTANGLE);
	reactangle.draw();
	
	
	float  length = sc.nextFloat(), width  = sc.nextFloat();
	float perimeter = 2 * (length + width), area3 = length * width;
	
	 System.out.println("Perimeter of rectangle is " + perimeter + " units.");
     System.out.println("Area of rectangle is " + area3 + " sq. units.");
	
	}    
      
     
}
